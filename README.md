# Jamaican Developers Site Theme

This theme was initially built via themeforrest before converted to a filesystem theme.
It is mainly for Plone sites, such as Jamaican Developers website, but can be used for
other sites as well.

## Theme development

To extend the theme, we strongly advice that you install nodejs and bower. Once bower
and nodejs are installed, get additional external resources for the theme by executing the
following command in the parent directory of the theme:

    sudo npm install -g bower
    bower install


### Install NPM Module

If you are going to make css changes to the theme, we strongly suggest you install less-watch-compile by
running the following command:

    sudo npm install -g less-watch-compiler

Afterwards, the following command to automatically compile less on change:

    less-watch-compiler css css styles.less
