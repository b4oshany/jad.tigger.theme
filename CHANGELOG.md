CHANGES
=================

v1.8.0
-----------------
- Mobile UI / UX improvements.
  @b4oshany


v1.7.1 - v1.7.3
----------------------
- UI / UX improvements
- Fix the content edit page for members only.
- Remove duplicate styles.css when the page renders.
  @b4oshany

v1.6.3 -- v1.7.0
----------------------
- UI / UX improvements
- Add a menu dropdown in the top toolbar.
- Fix the post page.
  @b4oshany

v1.6.2
--------------
- UI / UX improvements for Mosaic layouts. @b4oshany

v1.6.1
----------------
- UI / UX improvements for Lead Image and Metadata. @b4oshany

v1.6.0
-----------------
- Major UI / UX improvements
- Improve the footer styles.
- Set the style for Document portal type as the default style for all pages.
- Add a theme fragment that displays the portal type of the contexted item.
  @b4oshany

v1.5.3
----------------
- Fix the resolve uid generated from the Mosaic Theme Fragment Link Tile.
  @b4oshany

v1.5.2
-----------
- Improve button styles.
  @b4oshany

v1.5.0 - v1.5.1
----------------
- Add a button link theme fragment tile.
- Improve UI/UX for the grid_view layout.
- Add [masonry.js](https://masonry.desandro.com/) to the grid_view.
- Add a white background mosaic style to the theme's registry.
- Add mixins.less file.
  @b4oshany

v1.4.2
-----------------
- Improve the grid view layout. 
- Add margin-top to the footer heading that aren't the first child. 
  @b4oshany

v1.4.1
----------------
- Touches osoobe/jamaicandevelopers/jamaicandevelopers.site#53, Moved plone.portlet.collection.collection.pt from the jad.tigger.theme to jamaicandevelopers.site package. @b4oshany
- Improve the rules file for the footer section. 

v1.4.0
-----------------
- Closes #33, adds padding top and bottom to the grid view layout for mosaic tiles.
- Closes #35 , Add a flexible method to add new footer elements
- Closes osoobe/jamaicandevelopers/jamaicandevelopers.site#53, Allow a member photo to be accessible at member/@@images/image.


v1.3.0
-----------------
- Rebuild the homepage. @b4oshany
- Fixes tiles. @b4oshany
- Fixes css code breakage @b4oshany
- Adds theme based styles for tiles @b4oshany

v1.1.7 - v1.2.0
------------------
- Fixes osoobe/jamaicandevelopers/jamaicandevelopers.com#13, the landing page for a job and project by @b4oshany.
- Closes osoobe/jamaicandevelopers/jad.tigger.theme#28, display portlets and page content for mosaic views @b4oshany


v1.1.2 - v1.1.6
---------------------
- Update pages.xml Commited by @b4oshany
- Closes osoobe/jamaicandevelopers/jamaicandevelopers.site#42, osoobe/jamaicandevelopers/jamaicandevelopers.site#41, osoobe/jamaicandevelopers/jamaicandevelopers.site,   osoobe/jamaicandevelopers/jamaicandevelopers.site#4 and osoobe/jamaicandevelopers/jamaicandevelopers.site#40, allow users to add organisations, projects and jobs. Commited by @b4oshany

v1.1.1
--------------------
- Update portlet styles. by @b4oshany

v1.0.0 - v1.1.0
--------------------

- Closes #26, link find a collaborator section on the home page to the profiles page. by @b4oshany
- Fixed the pages.xml file. by @b4oshany
- Touches osoobe/jamaicandevelopers/jamaicandevelopers.site#39, set a default image when listing events by @b4oshany
- Closes #15, improve the search page. by @b4oshany
- Fixes jamaicandevelopers.site #14, search box is not focus when the search icon is clicked in the toolbar by @b4oshany
- Touches #osoobe/jamaicandevelopers/jamaicandevelopers.site#38, set the default value for effective date to the current time. by @b4oshany
- Update css/styles.less, css/styles.css files by @b4oshany
- Fixes #22, font awesome icons for the share button. by @b4oshany
- Added a floating button to file bugs. by @b4oshany
- Closes #20, update the links in the nav menu in mobile view. by @b4oshany
- Use effective date instead of creation date. by @b4oshany
- Added gogetfunding donation button. by @b4oshany
- UI improvements. by @b4oshany
- Closes osoobe/jamaicandevelopers/jad.tigger.theme#19, remove side toolbar for users that don't have the correct permission. by @b4oshany
- Touches #osoobe/jamaicandevelopers/jamaicandevelopers.site#31, allow left sidebar to be shown. by @b4oshany
- Closes #18, show plone alerts. by @b4oshany
- Closes #17, link nearby developers on the homepage to the profiles page. by @b4oshany
- Remove unnecessary section from the homepage. by @b4oshany
- Closes osoobe/jamaicandevelopers/jamaicandevelopers.site#26, fix the profiles layout. by @b4oshany
- Replace favicon.ico by @b4oshany
- Upload New File by @b4oshany
- Touches #15 and #16, improve tag cloud searches. by @b4oshany
- Closes #14, fix the issue of logo not display on every other page except the home page and improve mobile view. by @b4oshany
- Adds plone.alert.less file. by @b4oshany
- Closes #13, make the Document Content Type view consistent with rest of the site. by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Improve the styling for the tag cloud. by @b4oshany
- Improve the UI for the event listing. by @b4oshany
- Closes #1, place the calendar navigation on the right side of the header. by @b4oshany
- Separate the content type rules from the main rules. by @b4oshany
- Closes #12 and #7, fix the social media share links on the homepage and removed empty side bar. by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Update .gitlab-ci.yml by @b4oshany
- Closes #9, include the site actions in the footer by @b4oshany
- Moved template overrides to package. by @b4oshany
- Add .gitlab-ci.yml by @b4oshany
- Updated the styles. by @b4oshany
- Updated the events page. by @b4oshany
- Replace the thumb nail image with the largest image for news item. by @b4oshany
- Closes #3, Replace the theme comment section with plone commenting area by @b4oshany
- Update the readme. by @b4oshany
- Added images by @b4oshany
- Added includes.css by @b4oshany
- Update styles. by @b4oshany
- Update the manifest file. by @b4oshany
- Added other xml files. by @b4oshany
- Add the manifest.cfg file by @b4oshany
- Added the rules files. by @b4oshany
- Add the initial static files. by @b4oshany
