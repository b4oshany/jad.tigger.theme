def get_tags(self):
    subjects = self.context.portal_catalog.uniqueValuesFor("Subject")
    if len(subjects) > 15:
        return subjects[:15]
    return subjects