def effectivedate(self):
    from DateTime import DateTime
    zope_DT = DateTime()
    return {
        'date': zope_DT.strftime("%Y-%m-%d"),
        'time': zope_DT.strftime("%H:%M")
    }