def developers(context):
    portal = context.portal
    developers = portal["profiles"].listFolderContents(
        contentFilter={
            "portal_type" : ["jadmember"],
            'review_state': 'approved',
            'sort_limit': 6
    })
    
    return [{
              'link_class':"gl-link-{}".format(developer.id),
              'url': developer.absolute_url(),
              'title': "{}".format(developer.title),
              'description': developer.description,
              'has_photo': developer.portrait is not None
    } for developer in developers]