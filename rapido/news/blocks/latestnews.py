def results(context):
    content = context.portal['news']["aggregator"]
    items = content.queryCatalog()

    return [{
              'link_class':"gl-link-{}".format(item.id),
              'url':item.getPath(),
              'title': "{}".format(item.title),
              'description': item.description
    } for item in items]