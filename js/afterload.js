var $share_els = $(".uou-share-story .social-icons, .inject-social-icon");
if ($share_els != undefined){
    $share_els.jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
    });
}