$(document).ready(function(){
    var msnry_els = document.querySelectorAll(".grid-view-data-masonry");
    var msnry_items = [];
    for(var msnry_i in msnry_els){
        msnry_items.push(new Masonry(msnry_els[msnry_i], {"itemSelector": ".result-item" }));
        console.log("Masonry for item " + msnry_i);
    }
});